from time import time
from algorithms.csp_heuristics import CSPHeuristics


class CSP(CSPHeuristics):
    def __init__(self, csp_model):
        self.model = csp_model
        self.null_value = csp_model.null_value
        self.domain = csp_model.domain
        self.variables = csp_model.variables
        self.constraints = csp_model.constraints
        self.constraint_peers = csp_model.constraint_peers

        self.time = 0
        self.iterations = 0
        self.backs = 0

        self.current_index = 0
        self.is_forward = True

    def validate(self, var):
        raise NotImplementedError

    def _next_step(self, is_forward):
        self.is_forward = is_forward
        self.iterations += 1

        if self.is_forward:
            self.current_index += 1
        else:
            self.current_index -= 1
            self.backs += 1

    def _set_value(self, var):
        if var.is_constant:
            return self.is_forward

        start = 0 if var.value == self.null_value else self.domain.index(var.value) + 1

        for i in range(start, len(self.domain)):
            var.value = self.domain[i]
            if self.validate(var):
                return True

        var.value = self.null_value
        return False

    def next_iteration(self):
        self.current_index = len(self.variables) - 1
        self.iterations = 0
        self.backs = 0
        self.is_forward = False

    def run_iteration(self):
        start = time()

        while 0 <= self.current_index < len(self.variables):
            is_forward = self._set_value(self.variables[self.current_index])
            self._next_step(is_forward)

        self.time = time() - start
        return self.current_index >= 0

    def run_recursion(self):
        start = time()
        result = self._recursion(0)
        self.time = time() - start
        return result

    def _recursion(self, index):
        self.iterations += 1

        if index == len(self.variables):
            return True

        var = self.variables[index]

        if var.is_constant:
            return self._recursion(index + 1)

        for value in self.domain:
            var.value = value
            if self.validate(var) and self._recursion(index + 1):
                return True
            self.backs += 1

        var.value = self.null_value
        return False
