from algorithms.csp import CSP


class BackTracking(CSP):
    def __init__(self, csp_model):
        super().__init__(csp_model)

    def validate(self, var):
        return self.model.validate(var)


if __name__ == '__main__':
    from model.futoshiki import FutoshikiModel
    from model.skyscrapper import SkyscrapperModel

    directory = '/drive/Projects/computer-science/artificial-intelligence-and-knowledge-' \
                'engineering/lab2/constraint-satisfaction-problem/data/'

    futo_file_name = 'test_futo_8_2.txt'

    sky_file_name = 'test_sky_4_0.txt'

    futushiki_model = FutoshikiModel(directory + futo_file_name)
    skyscrapper_model = SkyscrapperModel(directory + sky_file_name)

    # array = [5, 6, 3, 7, 2, 1, 8, 4,
    #          6, 7, 2, 3, 4, 8, 5, 1,
    #          7, 8, 1, 4, 5, 6, 2, 3,
    #          3, 2, 6, 8, 1, 5, 4, 7,
    #          4, 1, 5, 2, 6, 7, 3, 8,
    #          1, 3, 4, 5, 8, 2, 7, 6,
    #          2, 4, 8, 1, 7, 3, 6, 5,
    #          8, 5, 7, 6, 3, 4, 1, 2]
    # for var, v in zip(futushiki_model.variables, array):
    #     var.value = v

    csp = BackTracking(futushiki_model)
    # csp = BackTracking(skyscrapper_model)

    # csp.most_constrained_variable()
    # csp.most_constraining_variable()
    # csp.least_constraining_value()

    csp.run_recursion()
    print('iterations', csp.iterations)
    print('backs', csp.backs)
    print('time', csp.time)
    print()
    print(csp.model.board())
    print(csp.model.check_result())

    # i = 1
    # while csp.run_iteration():
    #     print('Solution', i)
    #     print('iterations', csp.iterations)
    #     print('backs', csp.backs)
    #     print('time', csp.time)
    #     print()
    #     print(csp.model.board())
    #     print(csp.model.check_result())
    #     csp.next_iteration()
    #     i += 1
    #     break
