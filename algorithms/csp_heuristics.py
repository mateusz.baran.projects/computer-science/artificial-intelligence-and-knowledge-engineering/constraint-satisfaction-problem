class CSPHeuristics:
    model = None
    null_value = None
    variables = None
    constraints = None
    constraint_peers = None

    def _count_valid_values(self, var):
        if var.is_constant:
            return 0

        total = 0
        for v in var.domain:
            var.value = v
            if self.model.validate(var):
                total += 1
            var.value = self.null_value

        return total

    def _sum_valid_values(self, var, value):
        if var.is_constant:
            return 0

        total = 0
        start = self.variables.index(var) + 1
        var.value = value

        for i in range(start, len(self.variables)):
            v = self.variables[i]
            total += self._count_valid_values(v)

        var.value = self.null_value
        return total

    def most_constrained_variable(self):
        self.variables = sorted(self.variables,
                                key=self._count_valid_values)

    def most_constraining_variable(self):
        variables = []
        for _ in range(len(self.variables)):
            var = max(self.variables, key=lambda v: len(self.constraint_peers[v]))
            self.variables.pop(self.variables.index(var))
            if not var.is_constant:
                var.value = 1
            variables.append(var)

        self.variables = variables
        for var in self.variables:
            if not var.is_constant:
                var.value = self.null_value

        print(self.model.board())

    def least_constraining_value(self):
        for var in self.variables:
            var.domain = sorted(var.domain,
                                key=lambda v: self._sum_valid_values(var, v),
                                reverse=True)
