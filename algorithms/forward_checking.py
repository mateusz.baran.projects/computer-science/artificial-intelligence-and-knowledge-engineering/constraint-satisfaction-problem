from algorithms.csp import CSP


class ForwardChecking(CSP):
    def __init__(self, csp_model):
        super().__init__(csp_model)

    def validate(self, var):
        if not self.model.validate(var):
            return False

        for var in self.constraint_peers[var]:
            if not (var.value != self.null_value or var.is_constant or self.has_valid_value(var)):
                return False

        return True

    def has_valid_value(self, var):
        var.save()
        for v in self.domain:
            var.value = v
            has_valid_value = self.model.validate(var)

            if has_valid_value:
                var.restore()
                return True

        var.restore()
        return False


if __name__ == '__main__':
    from model.futoshiki import FutoshikiModel
    from model.skyscrapper import SkyscrapperModel

    directory = '/drive/Projects/computer-science/artificial-intelligence-and-knowledge-' \
                'engineering/lab2/constraint-satisfaction-problem/data/'

    futo_file_name = 'test_futo_7_2.txt'

    sky_file_name = 'test_sky_6_4.txt'

    futushiki_model = FutoshikiModel(directory + futo_file_name)
    skyscrapper_model = SkyscrapperModel(directory + sky_file_name)

    csp = ForwardChecking(futushiki_model)
    # csp = ForwardChecking(skyscrapper_model)

    # csp.most_constrained_variable()
    # csp.most_constraining_variable()
    # csp.least_constraining_value()

    i = 1
    while csp.run():
        print('Solution', i)
        print('iterations', csp.iterations)
        print('backs', csp.backs)
        print('time', csp.time)
        print()
        print(csp.model.board())
        print(csp.model.check_result())
        csp.next_iteration()
        i += 1
