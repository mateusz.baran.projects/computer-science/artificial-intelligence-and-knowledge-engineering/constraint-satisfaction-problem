class Variable:
    def __init__(self, value, is_constant):
        self.value = value
        self.is_constant = is_constant
        self._value = value

    def save(self):
        self._value = self.value

    def restore(self):
        self.value = self._value


class Model:
    def __init__(self, null_value=0):
        self.null_value = null_value
        self.constraints = {}
        self.constraint_peers = {}
        self.variables = []
        self.domain = []

    def validate(self, var):
        for func in self.constraints.get(var, []):
            if not func():
                return False
        return True

    def add_constraint(self, var, peers: iter, constraint):
        if not self.constraints.get(var):
            self.constraints[var] = []
            self.constraint_peers[var] = set()

        self.constraints[var].append(constraint)
        self.constraint_peers[var].update(peers)
