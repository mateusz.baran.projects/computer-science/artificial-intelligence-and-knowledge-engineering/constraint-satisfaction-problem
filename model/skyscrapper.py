from itertools import chain

from model.csp_model import Model, Variable


class SkyscrapperVariable(Variable):
    def __init__(self, value, is_constant=False):
        super().__init__(value, is_constant)

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return str(self.value)


class SkyscrapperModel(Model):
    def __init__(self, path, null_value=0):
        super().__init__(null_value)
        self.path = path
        self.top = []
        self.bottom = []
        self.right = []
        self.left = []
        self._init()

    def _init(self):
        with open(self.path) as file:
            self.data = file.readlines()
            self.size = int(self.data[0])
            self.domain = list(range(1, self.size + 1))

            for cell in str(self.data[1]).split(';')[1:]:
                self.top.append(int(cell))

            for cell in str(self.data[2]).split(';')[1:]:
                self.bottom.append(int(cell))

            for cell in str(self.data[3]).split(';')[1:]:
                self.left.append(int(cell))

            for cell in str(self.data[4]).split(';')[1:]:
                self.right.append(int(cell))

            for _ in range(self.size * self.size):
                self.variables.append(SkyscrapperVariable(0))

            for i, var in enumerate(self.variables):
                col = self.variables[i % self.size::self.size]
                index = int(i / self.size) * self.size
                row = self.variables[index:index + self.size]

                array = [v for v in chain(col, row) if v is not var]

                self.add_constraint(var, array, self.is_unique(var, array))

                left = self.left[int(i / self.size)]
                right = self.right[int(i / self.size)]
                if left != 0 and right != 0:
                    condition = self.is_correct_building(left, right, row)
                    self.add_constraint(var, row, condition)

                top = self.top[i % self.size]
                bottom = self.bottom[i % self.size]
                if top != 0 and bottom != 0:
                    condition = self.is_correct_building(top, bottom, col)
                    self.add_constraint(var, col, condition)

    def is_unique(self, x, array):
        def _is_unique():
            for y in array:
                if x.value == y.value:
                    return False

            return True

        return _is_unique

    def is_correct_building(self, left, right, array):
        def _is_correct_building():
            left_max = array[0].value
            right_max = array[-1].value
            left_sum = 1
            right_sum = 1

            for i in range(1, len(array)):
                if array[i].value == self.null_value or array[-1 - i].value == self.null_value:
                    return True

                if left_max < array[i].value:
                    left_max = array[i].value
                    left_sum += 1
                if right_max < array[-1 - i].value:
                    right_max = array[-1 - i].value
                    right_sum += 1

            if left != 0 and left_sum != left or right != 0 and right_sum != right:
                return False

            return True

        return _is_correct_building

    def board(self):
        board = ' ' * 3
        for c in self.top:
            board += f' _{c}_'
        board += '\n'

        for i in range(self.size):
            board += f' {self.left[i]} '
            for var in self.variables[i * self.size: i * self.size + self.size]:
                if i + 1 == self.size:
                    board += f'|_{var.value}_'
                else:
                    board += f'| {var.value} '
            board += f'| {self.right[i]}\n'

        board += ' ' * 3
        for c in self.bottom:
            board += f'  {c} '
        board += '\n'

        return board

    def check_result(self):
        result = ''
        for k, v in self.constraints.items():
            for func in v:
                if not func():
                    result += f'{str(k)} {func.__name__}\n'

        for var in self.variables:
            if var.value == self.null_value:
                result += f'{str(var)}\n'

        return result


if __name__ == '__main__':
    directory = '/drive/Projects/computer-science/artificial-intelligence-and-knowledge-' \
                'engineering/lab2/constraint-satisfaction-problem/data/'

    file_name = 'skyscrapper_4_0.txt'

    model = SkyscrapperModel(directory + file_name)

    print(model.size)
    print(model.variables)
    print('G', model.top)
    print('D', model.bottom)
    print('P', model.right)
    print('L', model.left)
    print()
    print(model.board())
