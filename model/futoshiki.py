from itertools import chain

from model.csp_model import Model, Variable


class FutoshikiVariable(Variable):
    def __init__(self, value, is_constant, row, col):
        super().__init__(value, is_constant)
        self.default_value = value
        self.row = row
        self.col = col

    def __str__(self):
        return self.row + self.col + '=' + str(self.value) + '*' * int(self.is_constant)

    def __repr__(self):
        return str(self.value)


class FutoshikiModel(Model):
    def __init__(self, path, null_value=0, row_labels='ABCDEFGHI', col_labels='123456789'):
        super().__init__(null_value)
        self.row_labels = row_labels
        self.col_labels = col_labels
        self.path = path
        self.data = []
        self._init()

    def _init(self):
        with open(self.path) as file:
            self.data = file.readlines()
            self.size = int(self.data[0])
            self.domain = list(range(1, self.size + 1))

            for i, row in enumerate(self.data[2: 2 + self.size]):
                _row = []
                for j, cell in enumerate(str(row).split(';')):
                    value = int(cell)
                    is_constant = value != self.null_value
                    cell = FutoshikiVariable(value, is_constant,
                                             self.row_labels[i], self.col_labels[j])
                    self.variables.append(cell)

            for row in self.data[3 + self.size:]:
                if row == '\n':
                    break

                cell1, cell2 = row.split(';')

                row1 = self.row_labels.index(cell1[0])
                col1 = self.col_labels.index(cell1[1])
                obj1 = self.variables[row1 * self.size + col1]

                row2 = self.row_labels.index(cell2[0])
                col2 = self.col_labels.index(cell2[1])
                obj2 = self.variables[row2 * self.size + col2]

                self.add_constraint(obj1, [obj2], self.is_lower(obj1, obj2))
                self.add_constraint(obj2, [obj1], self.is_greater(obj2, obj1))

            for i, var in enumerate(self.variables):
                col = self.variables[i % self.size::self.size]
                index = int(i / self.size) * self.size
                row = self.variables[index:index + self.size]

                array = [v for v in chain(col, row) if v is not var]

                self.add_constraint(var, array, self.is_unique(var, array))

    def is_lower(self, x, y):
        def _is_lower():
            return y.value == self.null_value or x.value < y.value

        return _is_lower

    def is_greater(self, x, y):
        def _is_greater():
            return y.value == self.null_value or x.value > y.value

        return _is_greater

    def is_unique(self, x, array):
        def _is_unique():
            for y in array:
                if x.value == y.value:
                    return False

            return True

        return _is_unique

    def board(self):
        board = ' ' * 3
        for c in self.col_labels[:self.size]:
            board += f' _{c}_'
        board += '\n'

        for i in range(self.size):
            board += f' {self.row_labels[i]} '
            for var in self.variables[i * self.size: i * self.size + self.size]:
                board += f'| {var.value} '
            board += '|\n'

        return board

    def check_result(self):
        result = ''
        for k, v in self.constraints.items():
            for func in v:
                if not func():
                    result += f'{str(k)} {func.__name__}\n'

        for var in self.variables:
            if var.is_constant and var.value != var.default_value or var.value == self.null_value:
                result += f'{str(var)}\n'

        return result


if __name__ == '__main__':
    directory = '/drive/Projects/computer-science/artificial-intelligence-and-knowledge-' \
                'engineering/lab2/constraint-satisfaction-problem/data/'

    file_name = 'futoshiki_4_0.txt'

    model = FutoshikiModel(directory + file_name)

    print(model.size)
    print(model.domain)
    print(model.variables)
    print(model.constraints)
    print(model.board())
    print(model.check_result())
